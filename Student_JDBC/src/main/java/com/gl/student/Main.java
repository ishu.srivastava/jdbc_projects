package com.gl.student;

import com.gl.student.DB.StudentDB;
import com.gl.student.services.Student;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException {
        System.out.println("Student Student");

        StudentDB db=new StudentDB();
        db.createStudent();
        db.getAllStudent();
        db.findStudentById();
        db.updateStudentById();
        db.deleteStudentById();
    }
}