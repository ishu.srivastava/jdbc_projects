package com.gl.student.DB;

import com.gl.student.exception.StudentException;
import com.gl.student.services.Student;

import java.sql.*;
import java.util.Scanner;

public class StudentDB {

    private final String url = "jdbc:postgresql://localhost:5432/movie_db";
    private final String username = "postgres";
    private final String password = "ishu123";

    private final Connection conn = DriverManager.getConnection(this.url, this.username, this.password);

    private Statement stmt = conn.createStatement();

    Student s=new Student();
    Scanner sc=new Scanner(System.in);
    public StudentDB() throws SQLException {

    }
    public void createStudent() throws SQLException {
        try {
            StringBuilder string = new StringBuilder("INSERT INTO student(studentid,studentname,studentaddress,studentemail)");
            string.append("VALUES(?,?,?,?)");

            System.out.println("Please Enter the student Id");
            String stuId = sc.nextLine();
            System.out.println("Please Enter the Student Name");
            String stuName = sc.nextLine();
            System.out.println("Please Enter the Student Address");
            String stuAdd = sc.nextLine();
            System.out.println("Please Enter the Student Email");
            String stuEmail = sc.nextLine();

            PreparedStatement statement = conn.prepareStatement(string.toString());
            statement.setString(1, stuId);
            statement.setString(2, stuName);
            statement.setString(3, stuAdd);
            statement.setString(4, stuEmail);

            int rowInserted = statement.executeUpdate();
            if (rowInserted > 0) {
                System.out.println("A new Student inserted succesfully");
            }
        }
        catch (SQLException s){
            System.out.println("DB Error");
        }

    }
    public void getAllStudent() throws SQLException {
        try {
            StringBuilder string = new StringBuilder("SELECT * FROM student");

            ResultSet rs = stmt.executeQuery(string.toString());

            int count = 0;

            while (rs.next()) {
                String stuId = rs.getString("studentid");
                String stuName = rs.getString("studentname");
                String stuAdd = rs.getString("studentaddress");
                String stuEml = rs.getString("studentemail");

                String output = "Student #%d: %s - %s - %s - %s ";
                System.out.println(String.format(output, ++count, stuId, stuName, stuAdd, stuEml));
            }
        }
        catch (SQLException s){
            System.out.println("DB Error");
        }

    }
    public void findStudentById() throws SQLException {
        try {
            System.out.println("Please Enter the Student id");
            String studId = sc.nextLine();
            StringBuilder string = new StringBuilder("SELECT * FROM student where studentid=");
            string.append("'").append(studId).append("'");

            ResultSet rs = stmt.executeQuery(string.toString());

            while (rs.next()) {
                String stuId = rs.getString("studentid");
                String stuName = rs.getString("studentname");
                String stuAdd = rs.getString("studentaddress");
                String stuEml = rs.getString("studentemail");

                String output = "Student  %s - %s - %s - %s ";
                System.out.println(String.format(output, stuId, stuName, stuAdd, stuEml));
            }
        }
        catch (SQLException s){
            System.out.println("DB Error");
        }

    }

    public void updateStudentById() throws SQLException {
        try {
            System.out.println("Please Enter the Student Id which you want to update");
            String studId = sc.nextLine();
            StringBuilder string = new StringBuilder("UPDATE student SET studentid=?,studentname=?,studentaddress=?,studentemail=?");
            string.append("where studentid='").append(studId).append("'");

            System.out.println("Please Enter the student Id");
            String stuId = sc.nextLine();
            System.out.println("Please Enter the Student Name");
            String stuName = sc.nextLine();
            System.out.println("Please Enter the Student Address");
            String stuAdd = sc.nextLine();
            System.out.println("Please Enter the Student Email");
            String stuEmail = sc.nextLine();

            PreparedStatement statement = conn.prepareStatement(string.toString());
            statement.setString(1, stuId);
            statement.setString(2, stuName);
            statement.setString(3, stuAdd);
            statement.setString(4, stuEmail);

            int rowInserted = statement.executeUpdate();
            if (rowInserted > 0) {
                System.out.println("Student updated succesfully");
            }
        }
        catch (SQLException s){
            System.out.println("DB Error");
        }

    }

    public void deleteStudentById() throws SQLException {
        System.out.println("Please Enter the Student Id which you want to delete");
        String studId = sc.nextLine();
        StringBuilder string= new StringBuilder("DELETE FROM student where studentid=");
        string.append("'").append(studId).append("'");

        stmt.execute(string.toString());
        System.out.println("student deleted successfully "+studId);

    }

}
