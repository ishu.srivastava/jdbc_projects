package com.gl.jdbc.movie.db;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

import com.gl.jdbc.movie.entity.Movie;

public class MovieDB {

    private final String url = "jdbc:postgresql://localhost:5432/movie_db";
    private final String username = "postgres";
    private final String password = "ishu123";

    private final Connection conn = DriverManager.getConnection(this.url, this.username, this.password);

    private Statement stmt = conn.createStatement();

    public MovieDB() throws SQLException {
    }

    Scanner sc=new Scanner(System.in);
    public void addMovie(Movie m) throws SQLException {
//        StringBuilder string1 = new StringBuilder("INSERT INTO movie VALUES('M1001', 'Home Alone', 'English', 1990, 476700000);");
//        stmt.execute(string1.toString());
//        StringBuilder string2 = new StringBuilder("INSERT INTO movie VALUES('M1002', 'Home Alone', 'English', 1992, 358900000);");
//        stmt.execute(string2.toString());
//        StringBuilder string3 = new StringBuilder("INSERT INTO movie VALUES('M1003','Matrix','English',1994, 16800000);");
//        stmt.execute(string3.toString());
//
////        string.append("Values ('").append(m.setMovieId(sc.nextLine())).append("','").append(m.setMovieName(sc.nextLine())).append("','").append(m.setLanguage(sc.nextLine())).append("',")
////                .append(m.setReleaseIn(sc.nextInt())).append(",").append(m.setRevenueInDoller(sc.nextLong())).append(")");
//        System.out.println("Data Added");
//
//
//            conn.close();
    }

    public void getMovie(Movie m) throws SQLException {
//        System.out.print("Please enter the release date =");
//        int year= sc.nextInt();
//        ArrayList<Movie> movie = new  ArrayList<Movie>();
//        StringBuilder string = new StringBuilder("select * from movie where releasedin >");
//        string.append(year).append(";");
//        ResultSet rs=stmt.executeQuery(string.toString());
//
//        while (rs.next()) {
//            m.setMovieId(rs.getString("movieid"));
//            m.setMovieName(rs.getString("moviename"));
//            m.setLanguage(rs.getString("language"));
//            m.setReleaseIn(rs.getInt("releasedin"));
//            m.setRevenueInDoller(rs.getLong("revenueindollars "));
//            movie.add(m);
//            System.out.println(m.getMovieId()+":"+m.getMovieName()+":"+m.getLanguage()+":"+m.getReleaseIn()+":"+m.getRevenueInDoller());
//        }
//        rs.close();
//        conn.close();

    }


    public void updateRevenue(Movie m) throws SQLException {
        System.out.println("Inside update revenue");
        System.out.print("Please Enter the movieId: ");
        String movieId=sc.nextLine();
        System.out.print("Please Enter the updated revenue: ");
        long revenue= sc.nextLong();

        StringBuilder string = new StringBuilder("update movie set revenueindollars ="+revenue+ "where movieid ='" +movieId+"'");
        stmt.executeQuery(string.toString());
        conn.close();
        System.out.println("Data updated Succesfully");
    }

    public void deleteMovie(Movie m) throws SQLException {
        System.out.print("Plase enter the movie id which you want to delete: ");
        String movieId=sc.nextLine();
        StringBuilder string = new StringBuilder("delete from movie where movieid ='"+movieId+"'");
        stmt.execute(string.toString());
        conn.close();
        System.out.println("Data deletd Successfully");
    }
}
